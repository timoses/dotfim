module dotfim.cmd.list;

import std.path : asRelativePath;
import std.range : array;
import std.stdio : writeln;

import dotfim.dotfim;

struct List
{
    DotfileManager dfm;

    this(lazy DotfileManager dfm)
    {
        this.dfm = dfm;

        exec();
    }

    private void exec()
    {
        writeln("Managed files:");
        foreach(gitdot; this.dfm.gitdots)
        {
            if (gitdot.managed)
                writeln("\t" ~ asRelativePath(gitdot.git.file,
                        this.dfm.settings.gitdir).array);
        }
    }
}
