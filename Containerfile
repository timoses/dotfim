FROM ubuntu AS build

ARG DVERSION=2.100.2

RUN apt-get update && apt-get -y install curl wget xz-utils build-essential git

# DMD and DUB
RUN curl -L -o ~/dvm https://github.com/jacob-carlborg/dvm/releases/download/v0.4.4/dvm-0.4.4-linux-debian7-x86_64 \
    && chmod +x ~/dvm \
    && ~/dvm install dvm \
SHELL ["bash"]
RUN ~/.dvm/bin/dvm install $DVERSION \
    && ~/.dvm/bin/dvm use $DVERSION \
    && chmod +x ~/.dvm/compilers/dmd-$DVERSION/linux/bin/dub


WORKDIR /build

COPY dub.sdl .
COPY dub.selections.json .
RUN ~/.dvm/compilers/dmd-$DVERSION/linux/bin/dub upgrade

COPY ./source/ ./source
COPY ./import ./import
ARG DUB_ARGS=--build=release
RUN ~/.dvm/compilers/dmd-$DVERSION/linux/bin/dub build $DUB_ARGS


FROM docker.io/frolvlad/alpine-glibc

RUN apk add git vimdiff \
    && git config --global user.email "no@mail.address" \
    && git config --global user.name "DotfimTester" \
    && git config --global credential.helper "store" \
    && git config --global merge.tool "vimdiff"

COPY --from=build /build/dotfim /bin

WORKDIR /dotfim/git

ENTRYPOINT ["dotfim"]

