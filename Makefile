

prepare:
	mkdir -p import && git describe --dirty --tags > import/VERSION

test:
	dub test

build: prepare
	podman build -t docker.io/timoses/dotfim:$(shell cat import/VERSION) -f Containerfile  .
	podman build --build-arg DUB_ARGS=--config=debug -t docker.io/timoses/dotfim:$(shell cat import/VERSION)-debug -f Containerfile  .

push: build
	podman push docker.io/timoses/dotfim:$(shell cat import/VERSION)
	podman push docker.io/timoses/dotfim:$(shell cat import/VERSION)-debug

build-latest:
	podman build -t docker.io/timoses/dotfim:latest -f Containerfile  .

push-latest: build-latest
	podman push docker.io/timoses/dotfim:latest


.PHONY: prepare test build build-latest push push-latest
